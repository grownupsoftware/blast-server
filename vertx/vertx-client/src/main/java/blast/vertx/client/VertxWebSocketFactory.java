/*
 * Grownup Software Limited.
 */
package blast.vertx.client;

import static blast.BlastConstants.BLAST_ROUTE_PATH;
import blast.server.config.BlastPropertiesImpl;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.WebSocket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author dhudson - Mar 27, 2017 - 12:22:31 PM
 */
public class VertxWebSocketFactory {

    private final HttpClient httpClient;
    private final Vertx vertx;

    public VertxWebSocketFactory() {
        vertx = Vertx.vertx();
        // Get default properties
        BlastPropertiesImpl properties = new BlastPropertiesImpl();
        properties.load();
        HttpClientOptions options = new HttpClientOptions();
        // Set the default port
        options.setDefaultPort(properties.getEndpoint().getPort());

        // Socket Conditioning
        options.setKeepAlive(properties.isKeepAlive());
        options.setReuseAddress(properties.isReuseAddress());
        options.setTcpNoDelay(properties.isNoDelay());

        // Note the IO swap here, is that properties are for a server and not a client.
        options.setSendBufferSize(properties.getInputBufferSize());
        options.setReceiveBufferSize(properties.getOutputBufferSize());
        options.setMaxPoolSize(1000);
        
        httpClient = vertx.createHttpClient(options);
    }

    public CompletableFuture<WebSocket> getWebSocket() {
        CompletableFuture<WebSocket> future = new CompletableFuture<>();

        httpClient.websocket(BLAST_ROUTE_PATH, (WebSocket e) -> {
            future.complete(e);
        });

        return future;
    }

    public WebSocket getWebSocketNow() {
        try {
            return getWebSocket().get();
        } catch (ExecutionException | InterruptedException ex) {
            return null;
        }

    }
}
