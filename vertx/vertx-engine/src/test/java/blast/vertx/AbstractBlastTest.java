/*
 * Grownup Software Limited.
 */
package blast.vertx;

import blast.Blast;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import blast.module.echo.EchoModule;
import blast.module.ping.PingModule;
import blast.server.BlastServer;
import blast.vertx.engine.VertxEngine;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 *
 * @author grant
 */
public class AbstractBlastTest {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private static BlastServer theBlastServer = null;
    private static int thePort;
    protected CountDownLatch theMessageLatch;
    public static ObjectMapper mapper;

    @BeforeClass
    public static void setup() {

        try {
            System.setProperty("blast-server.config", "blast-test-server.json");
            theBlastServer = startBlastServer();
            mapper = theBlastServer.getObjectMapper();
            thePort = theBlastServer.getProperties().getEndpoint().getPort();
        } catch (BlastException ex) {
            logger.error("Failed to start the server", ex);
            sleep();
        }
    }

    @AfterClass
    public static void shutdown() {
        if (theBlastServer != null) {
            // sleep - sometimes I miss log messages
            sleep();
            theBlastServer.shutdown();
        }
    }

    private URI getTestWsEndpoint() throws BlastException {
        try {
            return new URI("ws://127.0.0.1:" + thePort + "/blast");
            // use below to test connection errors
            // return new URI("ws://127.0.0.1:8081/blast");
        } catch (URISyntaxException ex) {
            logger.error("URI format exception", ex);
            throw new BlastException("URI format exception", ex);
        }
    }

    private static BlastServer startBlastServer() throws BlastException {
        try {

            BlastServer blast = Blast.blast(new VertxEngine(), new PingModule(), new EchoModule());

            logger.info("starting with properties: {}", blast.getProperties());
            blast.startup();

            // give the server a chance to start
            sleep();

            return blast;
        } catch (BlastException ex) {
            logger.error("Can't start Blast!", ex);
            throw ex;
        }

    }

    public String sendCommand(String command, String message) {
        return String.format("  { \"cmd\" : \"%s\", \"message\" : \"%s\" }", command, message);
    }

    public String sendCommand(String command) {
        return String.format("  { \"cmd\" : \"%s\" }", command);
    }

    // I find that when errors occur I can miss the last log message - so I sleep to ensure I get it
    public static void sleep() {
        BlastUtils.sleep(2000);
    }

    public BlastServer getBlastServer() {
        return theBlastServer;
    }
}
