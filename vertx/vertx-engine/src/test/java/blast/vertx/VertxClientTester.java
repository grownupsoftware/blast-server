/*
 * Grownup Software Limited.
 */
package blast.vertx;

import blast.utils.BlastUtils;
import blast.module.echo.EchoMessage;
import org.junit.Test;
import blast.vertx.client.VertxWebSocketFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.vertx.core.http.WebSocket;

/**
 *
 * @author dhudson - Mar 27, 2017 - 12:54:06 PM
 */
public class VertxClientTester extends AbstractBlastTest {

    @Test
    public void testEcho() {
        VertxWebSocketFactory factory = new VertxWebSocketFactory();
        WebSocket webSocket = factory.getWebSocketNow();

        getBlastServer().blast("Hello from Blast");

        EchoMessage echo = new EchoMessage();
        echo.setCmd("echo");
        echo.setMessage("Hello from a Vertx Client");

        try {
            webSocket.writeFinalTextFrame(mapper.writeValueAsString(echo));
        } catch (JsonProcessingException ex) {
            logger.warn("Unable to write JSON message", ex);
        }

        BlastUtils.sleep(2000);

    }

}
