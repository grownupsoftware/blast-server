/*
 * Grownup Software Limited.
 */
package blast.vertx;

import blast.exception.BlastException;
import blast.utils.BlastUtils;
import blast.stress.StressMessage;
import blast.stress.StressTestModule;
import blast.stress.VertxClientWebSocket;
import static blast.vertx.AbstractBlastTest.logger;
import blast.vertx.client.VertxWebSocketFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.netty.util.internal.ThreadLocalRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.Test;

/**
 *
 * @author dhudson - Mar 27, 2017 - 1:50:06 PM
 */
public class StressTest extends AbstractBlastTest {

    private static final int NO_OF_CLIENTS = 10;
    private final List<VertxClientWebSocket> clients = new ArrayList<>(NO_OF_CLIENTS);

    @Test
    public void stressTest() throws BlastException {

        StressTestModule module = new StressTestModule();

        // This will call configure
        getBlastServer().registerModule(module);

        module.startup();

        VertxWebSocketFactory factory = new VertxWebSocketFactory();

        // See how fast we can create ..
        for (int i = 0; i < NO_OF_CLIENTS; i++) {
            clients.add(new VertxClientWebSocket(factory.getWebSocketNow(), clients));
        }

        logger.info("Finished Creating Clients ...");

        logger.info("Blasting message ...");
        for (int i = 0; i < 10; i++) {
            long start = System.currentTimeMillis();
            getBlastServer().blast("Hello from Blast");
            long end = System.currentTimeMillis();
            logger.info("Blasted in {}ms", end - start);
        }

        StressMessage message = new StressMessage();

        while (!clients.isEmpty()) {
            message.setUuid(UUID.randomUUID().toString());
            try {
                getRandomClient().write(mapper.writeValueAsString(message));
            } catch (JsonProcessingException ignore) {
                // Ignore
            }
        }

        logger.info("Just 'aving a fag ....");
        BlastUtils.sleep(20000);

        logger.info("Closing down ...");
        getBlastServer().shutdown();
    }

    private VertxClientWebSocket getRandomClient() {
        if (clients.isEmpty()) {
            return null;
        }

        VertxClientWebSocket websocket;
        synchronized (clients) {
            websocket = clients.get(ThreadLocalRandom.current().nextInt(clients.size()));
        }
        return websocket;
    }

}
