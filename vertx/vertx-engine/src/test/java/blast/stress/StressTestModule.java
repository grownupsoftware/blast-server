/*
 * Grownup Software Limited.
 */
package blast.stress;

import blast.client.BlastServerClient;
import blast.client.ClosingReason;
import blast.Controllable;
import blast.command.CommandModule;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import blast.eventbus.OnEvent;
import blast.server.ServerClosingEvent;
import blast.module.BlastModule;
import blast.server.BlastServer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.util.internal.ThreadLocalRandom;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author dhudson - Mar 27, 2017 - 2:32:14 PM
 */
public class StressTestModule extends Thread implements BlastModule, Controllable, Runnable {

    private static final BlastLogger logger = BlastLogger.createLogger();

    private BlastServer blastServer;
    private boolean isRunning;
    private ObjectMapper mapper;

    public StressTestModule() {
        setName("StressTest Module");
    }

    @Override
    public void configure(BlastServer server) throws BlastException {
        blastServer = server;
        server.getEventBus().register(this);

        CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);
        commandModule.registerCommand("stress", StressEvent.class, StressMessage.class);
    }

    @Override
    public void startup() throws BlastException {
        start();
    }

    @Override
    public void shutdown() {
        isRunning = false;
    }

    @Override
    public void run() {
        isRunning = true;
        // We just want to do random things for a while.
        while (isRunning) {

            BlastServerClient client = getRandomClient();

            if (client != null) {
                switch (ThreadLocalRandom.current().nextInt(10)) {
                    // Two in 10 chance ..
                    case 0:
                    case 1:
                        client.close(ClosingReason.CLOSED_BY_SERVER);
                        break;

                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        StressMessage message = new StressMessage();
                        message.setUuid(UUID.randomUUID().toString());
                        try {
                            client.queueMessage(mapper.writeValueAsBytes(message));
                        } catch (JsonProcessingException ex) {
                            logger.warn(ex);
                        }
                        break;

                }
            }
            BlastUtils.sleep(500);
        }
    }

    @OnEvent
    public void serverClosingHandler(ServerClosingEvent closingEvent) {
        shutdown();
    }

    @OnEvent
    public void stressEventHandler(StressEvent event) {
        logger.info("I have a message from {} of {}", event.getClient(), event.getCommandData().getUuid());
    }

    public BlastServerClient getRandomClient() {
        List<BlastServerClient> clients = blastServer.getClients();
        if (clients.isEmpty()) {
            return null;
        }

        return clients.get(ThreadLocalRandom.current().nextInt(clients.size()));
    }

    @Override
    public String getModuleID() {
        return "co.gusl.stress.test";
    }
}
