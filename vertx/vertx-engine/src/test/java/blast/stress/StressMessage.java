/*
 * Grownup Software Limited.
 */
package blast.stress;

import blast.command.CommandMessage;

/**
 *
 * @author dhudson - Mar 27, 2017 - 2:52:51 PM
 */
public class StressMessage extends CommandMessage {

    public String uuid;

    public StressMessage() {
        super("stress");
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
