/*
 * Grownup Software Limited.
 */
package blast.stress;

import blast.log.BlastLogger;
import io.vertx.core.Handler;
import io.vertx.core.http.WebSocket;
import java.util.List;

/**
 *
 * @author dhudson - Mar 27, 2017 - 11:29:07 AM
 */
public class VertxClientWebSocket {

    private static final BlastLogger logger = BlastLogger.createLogger();

    private final WebSocket webSocket;
    private final List<VertxClientWebSocket> peers;

    public VertxClientWebSocket(WebSocket webSocket, List<VertxClientWebSocket> peers) {
        this.webSocket = webSocket;
        this.peers = peers;
        webSocket.textMessageHandler(new Handler<String>() {
            @Override
            public void handle(String e) {
                logger.info("Have message of .. {}", e);
            }
        });
        webSocket.closeHandler(new Handler<Void>() {
            @Override
            public void handle(Void e) {
                synchronized (peers) {
                    peers.remove(VertxClientWebSocket.this);
                }
            }
        });
    }

    public void close() {
        webSocket.close();
    }

    public void write(String bytes) {
        try {
            webSocket.writeFinalTextFrame(bytes);
        } catch (IllegalStateException ignore) {
            // Its because the connection is closed.
        }
    }
}
