/*
 * Grownup Software Limited.
 */
package blast.vertx.engine;

import blast.Controllable;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.module.BlastModule;
import blast.server.BlastServer;
import blast.server.config.BlastProperties;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;

/**
 *
 * @author dhudson - Mar 20, 2017 - 12:07:14 PM
 */
public class VertxEngine implements BlastModule, Controllable {

    public static final String VERTX_ENGINE_MODULE_ID = "co.gusl.vertx.engine";

    private static BlastLogger logger = BlastLogger.createLogger();

    private Vertx vertx;
    private HttpServer httpServer;

    @Override
    public void configure(BlastServer server) throws BlastException {
        // Should work in a cluster
        //vertx = Vertx.clusteredVertx(options, resultHandler)

        BlastProperties properties = server.getProperties();

        // TODO: ..  Some stuff here depending on BlastProperties 
        vertx = Vertx.vertx(new VertxOptions());

        HttpServerOptions options = new HttpServerOptions();
        options.setAcceptBacklog(properties.getEndpoint().getBacklog());
        options.setPort(properties.getEndpoint().getPort());
        // Don't time out, need to do something with ping here
        options.setIdleTimeout(0);
        options.setReuseAddress(properties.isReuseAddress()).setReceiveBufferSize(properties.getInputBufferSize());
        options.setSendBufferSize(properties.getOutputBufferSize()).setSoLinger(properties.getSoLinger());
        options.setTcpNoDelay(properties.isNoDelay());

        // Make into a debug option
        options.setLogActivity(true);

        //TODO: Make number of instances the same as the Inbound Thread Pool
        httpServer = vertx.createHttpServer(options);
        httpServer.requestHandler(new BlastRequestHandler(server));
    }

    @Override
    public void startup() {
        httpServer.listen();
        logger.info("Starting Blast Engine (Vertx) on port {}", httpServer.actualPort());
    }

    @Override
    public void shutdown() {
        if (vertx != null) {
            vertx.close();
        }
    }

    @Override
    public String getModuleID() {
        return VERTX_ENGINE_MODULE_ID;
    }

}
