/*
 * Grownup Software Limited.
 */
package blast.undertow.server;

import blast.Blast;
import blast.exception.BlastException;
import blast.module.echo.EchoModule;
import blast.module.ping.PingModule;
import blast.server.BlastServer;
import blast.undertow.engine.UndertowEngine;

/**
 * Undertow Bootstrap Class.
 *
 * @author dhudson - Mar 24, 2017 - 1:44:34 PM
 */
public class BlastUndertowServer {

    public static void main(String[] args) {
        try {

            BlastServer blast = Blast.blast(new UndertowEngine(), new PingModule(),
                    new EchoModule());

            blast.startup();
        } catch (BlastException ex) {
            Blast.logger.warn("Can't start Blast!", ex);
        }
    }
}
