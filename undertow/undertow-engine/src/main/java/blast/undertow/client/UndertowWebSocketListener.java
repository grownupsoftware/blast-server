/*
 * Grownup Software Limited.
 */
package blast.undertow.client;

import blast.client.ClosingReason;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.StreamSourceFrameChannel;
import io.undertow.websockets.core.WebSocketChannel;
import java.io.IOException;

/**
 * Web Socket Listener.
 *
 * @author dhudson - Mar 24, 2017 - 2:17:23 PM
 */
public class UndertowWebSocketListener extends AbstractReceiveListener {
    
    private final UndertowServerClient serverClient;
    
    public UndertowWebSocketListener(UndertowServerClient serverClient) {
        this.serverClient = serverClient;
    }
    
    @Override
    protected void onClose(WebSocketChannel webSocketChannel, StreamSourceFrameChannel channel) throws IOException {
        serverClient.postClientClosed(ClosingReason.CLOSED_BY_CLIENT);
    }
    
    @Override
    protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) throws IOException {
        serverClient.postClientInboundMessageEvent(message.getData());
    }
    
}
