/*
 * Grownup Software Limited.
 */
package blast.undertow.engine;

import blast.BlastConstants;
import blast.Controllable;
import blast.exception.BlastException;
import blast.module.BlastModule;
import blast.server.BlastServer;
import blast.server.config.BlastProperties;
import io.undertow.Undertow;

import static io.undertow.Handlers.path;
import static io.undertow.Handlers.websocket;

/**
 * Undertow Engine.
 *
 * @author dhudson - Mar 24, 2017 - 1:47:58 PM
 */
public class UndertowEngine implements BlastModule, Controllable {

    public static final String UNDERTOW_ENGINE_MODULE_ID = "co.gusl.undertow.engine";
    private Undertow server;
    private BlastServer blastServer;
    private BlastProperties properties;

    @Override
    public void configure(BlastServer server) throws BlastException {
        blastServer = server;
        properties = server.getProperties();
    }

    @Override
    public void startup() throws BlastException {
        server = Undertow.builder()
                .addHttpListener(properties.getEndpoint().getPort(), "localhost")
                .setHandler(path().addPrefixPath(BlastConstants.BLAST_ROUTE_PATH,
                        websocket(new BlastWebSocketConnectionCallback(blastServer)))).build();
        server.start();
    }

    @Override
    public void shutdown() {
        server.stop();
    }

    @Override
    public String getModuleID() {
        return UNDERTOW_ENGINE_MODULE_ID;
    }

}
