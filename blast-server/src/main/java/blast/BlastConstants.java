/*
 * Grownup Software Limited.
 */
package blast;

/**
 * Constants used by Blast.
 *
 * @author dhudson - Mar 29, 2017 - 12:19:57 PM
 */
public interface BlastConstants {

    /**
     * {@value }
     */
    public static final String BLAST_ROUTE_PATH = "/blast";

    /**
     * {@value }
     */
    public static final String ORIGIN_HEADER_KEY = "Origin";

    /**
     * {@value }
     */
    public static final String UPGRADE_HEADER_KEY = "Upgrade";

    /**
     * {@value }
     */
    public static final String UPGRADE_WEBSOCKET_VALUE = "websocket";

    /**
     * {@value }
     */
    public static final String X_FORWARDED_FOR_HEADER_KEY = "X-Forwarded-For";

    /**
     * {@value }
     */
    public static final String REMOTE_ADDRESS_KEY = "Blast-Remote-Address";

    /**
     * {@value }
     */
    public static final String REMOTE_PORT_KEY = "Blast-Remote-Port";

}
