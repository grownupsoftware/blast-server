/*
 * Grownup Software Limited.
 */
package blast.json;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Factory Class to create object mapper.
 *
 * Object Mappers are thread safe
 *
 * @author dhudson - Mar 15, 2017, 1:17:21 PM
 */
public class ObjectMapperFactory {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final TimeZone UTC_TZ = TimeZone.getTimeZone("UTC");

    private ObjectMapperFactory() {
    }

    /**
     * Create a Object Mapper with lots of options set.
     *
     * @return a mapper with the default options set.
     */
    public static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        setOptions(mapper);
        return mapper;
    }

    /**
     * Create a mapper with a given factory.
     *
     * @param factory
     * @return
     */
    public static ObjectMapper createObjectMapper(JsonFactory factory) {
        ObjectMapper mapper = new ObjectMapper(factory);
        setOptions(mapper);
        return mapper;
    }

    private static void setOptions(ObjectMapper mapper) {
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);
        mapper.setSerializationInclusion(Include.NON_EMPTY);

        // Configure deserializer
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        mapper.configure(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,
                DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
        mapper.enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME);
        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE,
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper = mapper.setAnnotationIntrospector(new AnnotationIntrospectorPair(
                new JacksonAnnotationIntrospector(),
                new JaxbAnnotationIntrospector(TypeFactory.defaultInstance())));
        mapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT));
        mapper.setTimeZone(UTC_TZ);

        // Java Time
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new AfterburnerModule());
    }

    public static void installBigDecimalTwoPlacesSerialiser(ObjectMapper mapper) {
        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new BigDecimalSerializer());
        mapper.registerModule(module);
    }
}
