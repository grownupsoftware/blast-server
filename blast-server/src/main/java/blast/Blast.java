/*
 * Grownup Software Limited.
 */
package blast;

import blast.json.ObjectMapperFactory;
import blast.log.BlastLogManager;
import blast.log.BlastLogger;
import blast.module.BlastModule;
import blast.server.BlastServer;
import blast.server.BlastServerImpl;
import blast.server.config.BlastProperties;
import blast.server.config.BlastPropertiesImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhudson - Mar 20, 2017 - 2:01:04 PM
 */
public class Blast {

    // Root Logger
    public static final BlastLogger logger = BlastLogManager.getLogger();

    // Hold Blast Servers by Key
    private static final Map<String, BlastServer> blastContainer = new HashMap<>(2);

    /**
     * Minimum Blast Server.
     *
     * Default configs will be used.
     *
     * @return
     */
    public static BlastServer blast() {
        return new Builder().build();
    }

    public static BlastServer blast(BlastModule... modules) {
        Builder builder = new Builder();
        for (BlastModule module : modules) {
            builder.installModule(module);
        }
        return builder.build();
    }

    public static BlastServer getBlastInstance(String key) {
        return blastContainer.get(key);
    }

    public static void storeBlastInstance(String key, BlastServer server) {
        blastContainer.put(key, server);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private BlastProperties properties;
        private final List<BlastModule> modules;
        private ObjectMapper objectMapper;

        Builder() {
            BlastPropertiesImpl jsonProperties = new BlastPropertiesImpl();
            jsonProperties.load();
            setProperties(jsonProperties);
            modules = new ArrayList<>(5);
            objectMapper = ObjectMapperFactory.createObjectMapper();
        }

        public Builder setProperties(BlastProperties properties) {
            this.properties = properties;
            return this;
        }

        public Builder setObjectMapper(ObjectMapper mapper) {
            this.objectMapper = mapper;
            return this;
        }

        public Builder installModule(BlastModule module) {
            modules.add(module);
            return this;
        }

        public BlastServer build() {
            BlastServerImpl blast = new BlastServerImpl();
            blast.setProperties(properties);
            blast.setObjectMapper(objectMapper);
            
            for (BlastModule module : modules) {
                blast.installModule(module);
            }

            return blast;
        }
    }
}
