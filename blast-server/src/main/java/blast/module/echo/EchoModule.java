/*
 * Grownup Software Limited.
 */
package blast.module.echo;

import blast.command.CommandModule;
import blast.exception.BlastException;
import blast.module.BlastModule;
import blast.server.BlastServer;

/**
 *
 * @author grant
 */
public class EchoModule implements BlastModule {

    public static final String ECHO_MODULE_ID = "co.gusl.echo";

    @Override
    public void configure(BlastServer server) throws BlastException {

        // Echo requires the Command Module
        CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

        // Echo command handler
        commandModule.registerCommand("echo", EchoCommandEvent.class, EchoMessage.class, new EchoCommandHandler());
    }

    @Override
    public String getModuleID() {
        return ECHO_MODULE_ID;
    }
}
