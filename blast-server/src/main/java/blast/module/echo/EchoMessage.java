/*
 * Grownup Software Limited.
 */
package blast.module.echo;

import blast.command.CommandMessage;

/**
 *
 * @author grant
 */
public class EchoMessage extends CommandMessage {

    private String message;

    public EchoMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "EchoMessage{" + "message=" + message + '}';
    }

}
