/*
 * Grownup Software Limited.
 */
package blast.module;

import blast.exception.BlastException;
import blast.server.BlastServer;

/**
 *
 * @author dhudson - Mar 23, 2017 - 4:37:44 PM
 */
public interface BlastModule {

    public void configure(BlastServer server) throws BlastException;
    
    public String getModuleID();

}
