/*
 * Grownup Software Limited.
 */
package blast.module.ping;

import blast.command.CommandMessage;

/**
 * Ping Message.
 *
 * This is the inbound JSON message
 *
 * @author dhudson - Mar 23, 2017 - 11:44:57 AM
 */
public class PingMessage extends CommandMessage {

    public PingMessage() {

    }
}
