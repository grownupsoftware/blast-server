/*
 * Grownup Software Limited.
 */
package blast.module.ping;

import blast.log.BlastLogger;
import blast.eventbus.OnEvent;

/**
 * This is what gets called by the event bus.
 *
 * @author dhudson - Mar 23, 2017 - 11:46:40 AM
 */
public class PingCommandHandler {

    private static final BlastLogger logger = BlastLogger.createLogger();

    public PingCommandHandler() {
    }

    @OnEvent
    public void handlePing(PingCommandEvent event) {
        logger.debug("I have event .. {}", event.getCommandData());
    }

}
