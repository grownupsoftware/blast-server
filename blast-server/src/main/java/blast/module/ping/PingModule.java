/*
 * Grownup Software Limited.
 */
package blast.module.ping;

import blast.command.CommandModule;
import blast.exception.BlastException;
import blast.module.BlastModule;
import blast.server.BlastServer;

/**
 *
 * @author dhudson - Mar 23, 2017 - 4:38:55 PM
 */
public class PingModule implements BlastModule {

    public static final String PING_MODULE_ID = "co.gusl.ping";

    @Override
    public void configure(BlastServer server) throws BlastException {
        // Ping requires the Command Module
        CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

        // Ping command handler
        commandModule.registerCommand("ping", PingCommandEvent.class, PingMessage.class, new PingCommandHandler());
    }

    @Override
    public String getModuleID() {
        return PING_MODULE_ID;
    }
}
