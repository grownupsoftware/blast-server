/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import blast.server.BlastServer;
import java.lang.reflect.Method;

/**
 *
 * @author dhudson - Apr 10, 2017 - 9:43:21 AM
 */
public class ListenerMethod implements Comparable<ListenerMethod> {

    private final Method method;
    private final OnEvent annotation;
    private final Object listener;
    private final boolean isWithServer;
    
    public ListenerMethod(Object listener, Method method, OnEvent annotation) {
        this.listener = listener;
        this.method = method;
        this.annotation = annotation;
        isWithServer = method.getParameterCount() == 2;
    }

    public int getOrder() {
        return annotation.order();
    }

    public Method getMethod() {
        return method;
    }

    public Object getListener() {
        return listener;
    }

    public void invoke(Object event, BlastServer server) throws Exception {
        Object[] args;
        
        if (isWithServer) {
            args = new Object[]{event, server};
        } else {
            args = new Object[]{event};
        }

        getMethod().invoke(listener, args);
    }

    @Override
    public int compareTo(ListenerMethod o) {
        return Integer.compare(getOrder(), o.getOrder());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof ListenerMethod) {
            ListenerMethod comp = ((ListenerMethod) obj);

            if (comp.getListener() != getListener()) {
                return false;
            }

            return ((ListenerMethod) obj).hashCode() == hashCode();
        }

        return false;
    }

    @Override
    public int hashCode() {
        return method.hashCode();
    }

    boolean isFailFast() {
        return annotation.failFast();
    }

}
