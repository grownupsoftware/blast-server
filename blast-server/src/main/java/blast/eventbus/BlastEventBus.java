/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import blast.Controllable;
import blast.module.BlastModule;

/**
 *
 * @author dhudson - Mar 23, 2017 - 8:49:32 AM
 */
public interface BlastEventBus extends Controllable, BlastModule {

    public void register(Object object);

    public void unregister(Object object);

    public void post(Object event);
}
