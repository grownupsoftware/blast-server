/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Event Bus Method tagging annotation.
 *
 * By default the order is set to 50, failFast is set to false.
 *
 * The description can be used by documentation systems, or just to give the
 * programmer a clue of what is going on.
 *
 * It is possible to add many listeners to the same event, if that is the case
 * then the listeners will be executed in order given.
 *
 * @author dhudson - Apr 10, 2017 - 8:31:17 AM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface OnEvent {

    int order() default 50;

    boolean failFast() default false;

    String description() default "";
}
