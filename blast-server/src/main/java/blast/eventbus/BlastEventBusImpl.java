/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import blast.Controllable;
import blast.exception.BlastException;
import blast.executors.NamedFixedPoolExecutor;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 *
 * @author dhudson - Apr 10, 2017 - 3:06:02 PM
 */
public class BlastEventBusImpl implements BlastEventBus, Controllable {

    public static final String EVENTBUS_MODULE_ID = "co.gusl.event.bus";

    private static final BlastLogger logger = BlastLogger.createLogger();

    private BlastServer blastServer;
    private final ListenerRegistra registra;
    private ExecutorService ebService;

    public BlastEventBusImpl() {
        registra = new ListenerRegistra();
    }

    @Override
    public void register(Object object) {
        registra.register(object);
    }

    @Override
    public void unregister(Object object) {
        registra.unregister(object);
    }

    @Override
    public void post(Object event) {
        List<ListenerMethod> listeners = registra.getListenersFor(event.getClass());
        if (listeners == null || listeners.isEmpty()) {
            // Nothing to do
            return;
        }
        ebService.execute(new BlastPostEvent(listeners, event, blastServer));
    }

    @Override
    public void configure(BlastServer server) throws BlastException {
        ebService = new NamedFixedPoolExecutor(server.getProperties().getEventBusThreadSize(), "Blast Event Bus");
    }

    @Override
    public void startup() throws BlastException {

    }

    @Override
    public void shutdown() {
        ebService.shutdown();
    }

    @Override
    public String getModuleID() {
        return EVENTBUS_MODULE_ID;
    }

}
