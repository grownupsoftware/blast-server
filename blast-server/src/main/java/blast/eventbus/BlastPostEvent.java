/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import static blast.Blast.logger;
import blast.exception.BlastException;
import blast.server.BlastServer;
import java.util.List;

/**
 * Immutable Runnable for Blast Events.
 *
 * Execute the listener methods in order, with the event given, watching for the
 * fail fast condition.
 *
 * @author dhudson - Apr 11, 2017 - 8:28:35 AM
 */
public class BlastPostEvent implements Runnable {

    private final List<ListenerMethod> listeners;
    private final Object event;
    private final BlastServer blastServer;

    BlastPostEvent(List<ListenerMethod> listeners, Object event, BlastServer server) {
        this.listeners = listeners;
        this.event = event;
        blastServer = server;
    }

    @Override
    public void run() {
        // I have the listeners in order
        for (ListenerMethod method : listeners) {
            try {
                method.invoke(event, blastServer);
            } catch (Throwable t) {
                logger.warn("EventBus caught exception, whilst tring to process {} for {}",
                        method.getMethod().getName(), method.getListener().getClass().getName(), t);
                // If its a Blast Exception and fail fast, just stop
                if (t instanceof BlastException && method.isFailFast()) {
                    return;
                }
            }
        }
    }

}
