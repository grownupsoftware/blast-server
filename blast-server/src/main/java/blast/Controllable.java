/*
 * Grownup Software Limited.
 */
package blast;

import blast.exception.BlastException;

/**
 *
 * @author dhudson - Mar 22, 2017 - 1:35:53 PM
 */
public interface Controllable {

    public void startup() throws BlastException;

    public void shutdown();
}
