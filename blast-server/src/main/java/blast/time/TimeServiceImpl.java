/*
 * Grownup Software Limited.
 */
package blast.time;

/**
 * Implementation of the TimeService.
 * 
 * Set the delta, (plus of minus) of what the current time is.
 * 
 * Created on Mar 15, 2017, 10:53:52 AM
 *
 * @author dhudson
 */
public class TimeServiceImpl implements TimeService {

    private long timeDelta = 0;

    public TimeServiceImpl() {
    }

    @Override
    public long getTime() {
        return System.currentTimeMillis() + timeDelta;
    }

    @Override
    public void setTimeDelta() {
        timeDelta = 0;
    }

}
