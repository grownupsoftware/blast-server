/*
 * Grownup Software Limited.
 */

package blast.time;

/**
 * Simple NTP like service.
 * 
 * Created on Mar 15, 2017, 10:53:31 AM
 * @author dhudson
 */
public interface TimeService {

    /**
     * Return the time in millis.
     * 
     * @return 
     */
    public long getTime();
    
    void setTimeDelta();
    
}
