/*
 * Grownup Software Limited.
 */

package blast.command;

/**
 *
 * @author dhudson - Mar 23, 2017 - 11:32:30 AM
 */
public interface CommandHandlerDetails {

    public String getCommand();
    
    public Class getMessagePojo();
    
    public Class getEventClass();
    
}
