/*
 * Grownup Software Limited.
 */
package blast.command;

import blast.client.BlastServerClient;

/**
 * Base Type for Command Events.
 * 
 * @author dhudson - Mar 23, 2017 - 12:26:03 PM
 * @param <T> Data Type
 */
public abstract class BlastCommandEvent<T> {

    private T commandData;
    private BlastServerClient serverClient;

    public BlastCommandEvent() {
    }

    public void setCommandData(T data) {
        commandData = data;
    }

    public T getCommandData() {
        return commandData;
    }

    public void setBlastServerClient(BlastServerClient client) {
        serverClient = client;
    }

    public BlastServerClient getClient() {
        return serverClient;
    }
}
