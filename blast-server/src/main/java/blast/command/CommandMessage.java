/*
 * Grownup Software Limited.
 */
package blast.command;

import blast.message.BlastMessage;

/**
 *
 * @author dhudson - Mar 22, 2017 - 11:01:58 AM
 */
public class CommandMessage implements BlastMessage {

    public String cmd;

    public CommandMessage() {
    }

    public CommandMessage(String cmd) {
        this.cmd = cmd;
    }

    @Override
    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

}
