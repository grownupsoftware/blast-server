/*
 * Grownup Software Limited.
 */
package blast.log;

import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.util.ReflectionUtil;

/**
 * Wrap the logging using a delegate so if we want to swap it out in the future
 * we can.
 *
 * @author dhudson - Mar 15, 2017 - 1:50:37 PM
 */
public class BlastLogManager {

    private static final HashMap<String, BlastLogger> theLoggers = new HashMap<>();

    /**
     * Returns a logger which may be newly created, or returned from the
     * repository.
     *
     * @param clazz name to create the logger
     * @return a logger
     */
    public static BlastLogger getLogger(Class<?> clazz) {
        return getLogger(clazz.getName());
    }

    /**
     * Returns a logger which may be newly created, or returned from the
     * repository.
     *
     * @param name
     * @return
     */
    public static BlastLogger getLogger(String name) {
        BlastLogger logger = theLoggers.get(name);

        if (logger == null) {
            logger = new BlastLogger(LogManager.getLogger(name));
            theLoggers.put(name, logger);
        }

        return logger;
    }

    /**
     * Returns a logger, with the name of the calling class, which may be newly
     * created or returned from the repository.
     *
     * @return a logger
     */
    public static BlastLogger getLogger() {
        return getLogger(ReflectionUtil.getCallerClass(2));
    }
}
