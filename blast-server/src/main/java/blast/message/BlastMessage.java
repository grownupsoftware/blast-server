/*
 * Grownup Software Limited.
 */
package blast.message;

/**
 *
 * @author dhudson
 */
public interface BlastMessage {

    public String getCmd();

}
