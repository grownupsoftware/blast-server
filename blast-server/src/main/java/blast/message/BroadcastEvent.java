/*
 * Grownup Software Limited.
 */
package blast.message;

import blast.client.BlastServerClient;
import blast.doc.BlastEvent;
import static blast.doc.BlastEvent.EventSource.APPLICATION;
import java.util.List;

/**
 * Broadcast Event.
 *
 * This is picked up by the client manager, to populate the list of connected
 * clients, and then by the Blaster to put the message on the queue for the
 * clients.
 *
 * @author dhudson - May 4, 2017 - 1:43:45 PM
 */
@BlastEvent(source = APPLICATION, description = "Broadcast event is listened to by the Client Manager (order 35), which populates the client list, and then the Blaster (order 40) to populate the queue.")
public class BroadcastEvent {

    private String message;
    private List<BlastServerClient> clients;

    public BroadcastEvent() {
    }

    public BroadcastEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BlastServerClient> getClients() {
        return clients;
    }

    public void setClients(List<BlastServerClient> clients) {
        this.clients = clients;
    }

}
