/*
 * Grownup Software Limited.
 */
package blast.client;

import static blast.BlastConstants.X_FORWARDED_FOR_HEADER_KEY;
import blast.utils.StringUtils;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

/**
 * Request Details Container.
 *
 * @author dhudson - Mar 28, 2017 - 4:15:39 PM
 */
public class WebSocketRequestDetailsImpl implements WebSocketRequestDetails {

    private Map<String, List<String>> headers;
    private Map<String, List<String>> params;
    private String queryString;
    private String requestPath;
    private String origin;
    private URI requestURI;
    private String remoteAddress;

    public WebSocketRequestDetailsImpl() {
    }

    @Override
    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    @Override
    public String getQueryString() {
        return queryString;
    }

    @Override
    public String getOrigin() {
        return this.origin;
    }

    @Override
    public Map<String, List<String>> getParams() {
        return params;
    }

    @Override
    public String getRequestPath() {
        return requestPath;
    }

    public void setHeaders(Map<String, List<String>> headers) {
        this.headers = headers;
    }

    public void setParams(Map<String, List<String>> params) {
        this.params = params;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public void setRequestPath(String resourcePath) {
        this.requestPath = resourcePath;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setRequestURI(URI uri) {
        requestURI = uri;
    }

    @Override
    public URI getRequestURI() {
        return requestURI;
    }

    @Override
    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String address) {
        remoteAddress = address;
    }

    private String getHeader(String key) {
        List<String> headerList = headers.get(key);
        if (headerList == null || headerList.isEmpty()) {
            return null;
        }

        return headerList.get(0);
    }

    /**
     * This will check the headers, and see if there is a X-Forwarded for
     */
    public void resolveRemoteAddress() {
        String remoteAddr = getHeader(X_FORWARDED_FOR_HEADER_KEY);

        if (remoteAddr != null) {
            if (remoteAddr.contains(",")) {
                // sometimes the header is of form client ip,proxy 1 ip,proxy 2 ip,...,proxy n ip,
                // we just want the client
                remoteAddr = StringUtils.split(remoteAddr, ',')[0].trim();
            }
            try {
                // If ip4/6 address string handed over, simply does pattern validation.
                InetAddress.getByName(remoteAddr);
                setRemoteAddress(remoteAddr);
            } catch (UnknownHostException ignore) {
            }
        }
    }
}
