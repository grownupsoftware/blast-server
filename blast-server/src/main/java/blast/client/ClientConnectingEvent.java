/*
 * Grownup Software Limited.
 */
package blast.client;

import blast.doc.BlastEvent;
import static blast.doc.BlastEvent.EventSource.CLIENT;

/**
 * Client Connecting Event.
 *
 * Any origin checking should be done on this event, and before the Client
 * Manager gets it.
 *
 * If for any reason, the connection is not valid, it is suggested that you
 * close the client and fail fast so that the client manager will not process
 * the client.
 *
 * @author dhudson - Mar 23, 2017 - 8:38:06 AM
 */
@BlastEvent(source = CLIENT, description = "This event is produced by the Engine, when a new client is created.  The Client Manager, will listener to this event and add the client."
        + "Any client validation should be done before the client manager, which has an order of 40.")
public class ClientConnectingEvent {

    private final BlastServerClient serverClient;

    public ClientConnectingEvent(BlastServerClient client) {
        serverClient = client;
    }

    public BlastServerClient getClient() {
        return serverClient;
    }
}
