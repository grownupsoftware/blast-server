/*
 * Grownup Software Limited.
 */
package blast.client;

import blast.Controllable;
import blast.module.BlastModule;
import java.util.List;

/**
 * Client Manager.
 *
 * Handles, err, the clients.
 *
 * @author dhudson - Mar 21, 2017 - 11:13:17 AM
 */
public interface ClientManager extends Controllable, BlastModule {

    public List<BlastServerClient> getClients();

    public BlastServerClient getClientByID(String id);
}
