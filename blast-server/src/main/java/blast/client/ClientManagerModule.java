/*
 * Grownup Software Limited.
 */
package blast.client;

import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.eventbus.BlastEventBus;
import blast.eventbus.OnEvent;
import blast.message.BroadcastEvent;
import blast.server.BlastServer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Client Manager Impl.
 *
 * @author dhudson - Mar 21, 2017 - 11:13:28 AM
 */
public class ClientManagerModule implements ClientManager {

    public static final String CLIENT_MANAGER_MODULE_ID = "co.gusl.client.manager";

    private static final BlastLogger logger = BlastLogger.createLogger();

    private final Map<String, BlastServerClient> clientMap;
    private BlastEventBus eventBus;

    public ClientManagerModule() {
        clientMap = Collections.synchronizedMap(new LinkedHashMap<>());
    }

    @Override
    public void configure(BlastServer server) {
        eventBus = server.getEventBus();
        eventBus.register(this);
    }

    private void addClient(BlastServerClient client) {
        if (client.getClientID() == null) {
            throw new RuntimeException("Client ID is null");
        }

        BlastServerClient oldClient = clientMap.put(client.getClientID(), client);

        if (oldClient != null) {
            // Already exists, so lets do the best we can.
            logger.warn("Adding client with an already exisiting client ID, old client will be removed and closed [{}]", oldClient.getClientID());
            oldClient.close(ClosingReason.CLOSED_BY_SERVER);
        }

        logger.info("Adding client {}:{}", client.getClass(), client.getClientID());
    }

    @Override
    public List<BlastServerClient> getClients() {
        return new ArrayList<>(clientMap.values());
    }

    private void removeClient(String clientID) {
        logger.debug("Closing client {}", clientID);
        clientMap.remove(clientID);
    }

    @Override
    public BlastServerClient getClientByID(String id) {
        return clientMap.get(id);
    }

    @Override
    public void startup() throws BlastException {
        // Register thyself
        eventBus.register(this);
    }

    @Override
    public void shutdown() {
        eventBus.unregister(this);

        clientMap.values().forEach((client) -> {
            client.close(ClosingReason.CLOSED_BY_SERVER);
        });

        clientMap.clear();
    }

    @OnEvent(order = 40)
    public void clientConnectingHandler(ClientConnectingEvent event) {
        addClient(event.getClient());
    }

    @OnEvent(order = 40)
    public void clientClosedHandler(ClientClosedEvent event) {
        removeClient(event.getClient().getClientID());
    }

    @OnEvent(order = 35)
    public void broadcast(BroadcastEvent event) {
        event.setClients(getClients());
    }

    @Override
    public String getModuleID() {
        return CLIENT_MANAGER_MODULE_ID;
    }

}
