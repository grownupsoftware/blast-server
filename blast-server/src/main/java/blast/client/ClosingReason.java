/*
 * Grownup Software Limited.
 */
package blast.client;

/**
 *
 * @author dhudson - Mar 23, 2017 - 9:00:18 AM
 */
public enum ClosingReason {

    CLOSED_BY_CLIENT,
    CLOSED_BY_SERVER,
    IO_EXCEPTION

}
