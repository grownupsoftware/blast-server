/*
 * Grownup Software Limited.
 */
package blast.client;

import blast.blaster.BlasterQueueEvent;
import blast.exception.BlastException;
import blast.eventbus.BlastEventBus;
import blast.server.BlastServer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dhudson - Mar 22, 2017 - 1:14:56 PM
 */
public abstract class AbstractBlastServerClient implements BlastServerClient {

    private final BlastEventBus eventBus;
    private final WebSocketRequestDetails details;
    private final ObjectMapper objectMapper;

    private final Map<String, Object> userProperties;

    public AbstractBlastServerClient(BlastServer server, WebSocketRequestDetails details) {
        eventBus = server.getEventBus();
        objectMapper = server.getObjectMapper();
        this.details = details;
        userProperties = new HashMap<>(1);
    }

    public void postClientConnectingEvent() {
        eventBus.post(new ClientConnectingEvent(this));
    }

    public void postClientInboundMessageEvent(String message) {
        eventBus.post(new ClientInboundMessageEvent(this, message));
    }

    public void postClientClosed(ClosingReason reason) {
        eventBus.post(new ClientClosedEvent(this, reason));
    }

    public void postQueuedMessageEvent(byte[] bytes) {
        eventBus.post(new BlasterQueueEvent(this, bytes));
    }

    @Override
    public WebSocketRequestDetails getRequestDetails() {
        return details;
    }

    @Override
    public String getRemoteAddress() {
        if (details == null) {
            return null;
        }

        return details.getRemoteAddress();
    }

    @Override
    public void queueMessage(Object message) throws BlastException {
        try {
            queueMessage(objectMapper.writeValueAsBytes(message));
        } catch (JsonProcessingException ex) {
            throw new BlastException("Failed to serialize outbound message", ex);
        }
    }

    public final String createShortClientID() {
        return BlastServerClientUtils.getShortClientID();
    }

    @Override
    public Map<String, Object> getUserProperties() {
        return userProperties;
    }

    @Override
    public Object getUserProperty(String key) {
        return userProperties.get(key);
    }

    @Override
    public void addUserProperty(String key, Object value) {
        userProperties.put(key, value);
    }

}
