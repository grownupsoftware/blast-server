/*
 * Grownup Software Limited.
 */
package blast.client;

import blast.doc.BlastEvent;
import static blast.doc.BlastEvent.EventSource.CLIENT;

/**
 * This is a message from a client.
 *
 * At this point, it has not been transformed into a command, its just raw text.
 *
 * @author dhudson - Mar 23, 2017 - 8:55:44 AM
 */
@BlastEvent(source = CLIENT, description = "This event is produced by the client, and will be consumed by the Command Message Decoder.")
public class ClientInboundMessageEvent {

    private final BlastServerClient client;
    private final String message;

    public ClientInboundMessageEvent(BlastServerClient client, String message) {
        this.client = client;
        this.message = message;
    }

    public BlastServerClient getClient() {
        return client;
    }

    public String getMessage() {
        return message;
    }

}
