/*
 * Grownup Software Limited.
 */
package blast.client;

import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Blast Server Client Utils.
 *
 * @author dhudson - Apr 4, 2017 - 2:49:34 PM
 */
public class BlastServerClientUtils {

    private static long sequence;
    private static long lastMillis;
    private static final ReentrantLock lock = new ReentrantLock(true);
    private static final long BLAST_EPOCH = 1491315987493L;

    private BlastServerClientUtils() {
    }

    /**
     * Return a UUID client ID.
     *
     * Its a long unique UUID, so its expensive.
     *
     * @return a long unique string
     */
    public static String getUUIDClientID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Return a shorter client ID based off CurrentMillis()
     *
     * @return a unique (for this cluster) id.
     */
    public static String getShortClientID() {
        long timestamp = System.currentTimeMillis();
        long tmp = 0;

        try {
            lock.lock();

            if (timestamp < lastMillis) {
                timestamp = lastMillis;
            }

            if (lastMillis == timestamp) {
                sequence++;
                if (sequence == 99) {
                    // Guard against 100 calls to generate ID in the same millis.
                    // Testing kind of proved that we never get here, but just incase we decide to run on a cray
                    timestamp = waitNextMillis(lastMillis);
                    lastMillis = timestamp;
                    sequence = 0;
                }
            } else {
                sequence = 0;
                lastMillis = timestamp;
            }

            tmp = sequence;
        } finally {
            lock.unlock();
        }

        // timestamp
        long id = timestamp - BLAST_EPOCH;
        // add two digit sequence
        id *= 100;
        id += tmp;

        return Long.toString(id, 32);
    }

    private static long waitNextMillis(long lastTimestamp) {
        //TODO: Lockpark nanos...
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }
}
