/*
 * Grownup Software Limited.
 */
package blast.executors;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Simple Thread Naming Factory.
 *
 * @author dhudson - Mar 17, 2017 - 10:47:05 AM
 */
public class NamedThreadFactory implements ThreadFactory {

    private final String name;
    private int threadCount;

    public NamedThreadFactory(String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = Executors.defaultThreadFactory().newThread(r);
        thread.setName(name + " " + threadCount++);
        thread.setPriority(Thread.NORM_PRIORITY + 1);
        return thread;
    }

}
