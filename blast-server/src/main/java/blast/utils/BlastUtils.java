/*
 * Grownup Software Limited.
 */
package blast.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

/**
 * Collection of static helper methods.
 *
 * @author dhudson - Mar 16, 2017 - 12:10:09 PM
 */
public class BlastUtils {

    // You know it makes sense
    public static final int TWELVE = 12;

    private BlastUtils() {
    }

    /**
     * Convenience method for getting a null safe stream from a collection
     *
     * @param collection
     * @return a a stream of the collection if not null, otherwise an empty
     * stream
     */
    public static <T> Stream<T> safeStream(Collection<T> collection) {
        return collection == null ? Stream.empty() : collection.stream();
    }

    public static <T> Stream<T> safeStream(T[] array) {
        return array == null ? Stream.empty() : Arrays.stream(array);
    }

    /**
     * Sleep for the given millis.
     *
     * @param time
     */
    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            //ignore
        }
    }
}
