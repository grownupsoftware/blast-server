/*
 * Grownup Software Limited.
 */
package blast.server;

/**
 *
 * @author dhudson - Mar 25, 2017 - 8:00:28 AM
 */
public class ServerEvent {

    private final BlastServer server;

    public ServerEvent(BlastServer server) {
        this.server = server;
    }

    public BlastServer getServer() {
        return server;
    }
    
}
