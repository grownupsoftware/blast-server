/*
 * Grownup Software Limited.
 */
package blast.server;

import blast.client.BlastServerClient;
import blast.Controllable;
import blast.exception.BlastException;
import blast.eventbus.BlastEventBus;
import blast.module.BlastModule;
import blast.server.config.BlastProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

/**
 *
 * Created on Mar 15, 2017, 10:36:42 AM
 *
 * @author dhudson
 */
public interface BlastServer extends Controllable {

    // Properties    
    public void setProperties(BlastProperties properties);

    public BlastProperties getProperties();

    // Object Mapper
    public ObjectMapper getObjectMapper();

    // Event Bus    
    public BlastEventBus getEventBus();

    public BlastServer registerEventListener(Object eventListener);

    /**
     * Send a message to each connected bytes.
     *
     * This could be Stringifed JSON or just text. The client will have to know
     * how to handle it.
     *
     * @param message
     */
    public void blast(String message);

    public void postEvent(Object event);
    
    // Modules ..
    public BlastServer registerModule(BlastModule module) throws BlastException;

    public BlastServer installModule(BlastModule module);
    
    public BlastModule getModule(String moduleID);
        
    public boolean hasModule(String ID);
    
    public BlastModule requireModule(String ID, Class<? extends BlastModule> module) throws BlastException;
    
    // Client Manager Module
    public List<BlastServerClient> getClients();

    public BlastServerClient getClientByID(String id);
}
