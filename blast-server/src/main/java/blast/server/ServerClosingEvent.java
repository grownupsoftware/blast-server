/*
 * Grownup Software Limited.
 */

package blast.server;

import blast.doc.BlastEvent;
import static blast.doc.BlastEvent.EventSource.SERVER;

/**
 * Server Closing Event.
 * 
 * @author dhudson - Mar 25, 2017 - 7:59:43 AM
 */
@BlastEvent(source = SERVER,description = "This event is produced by the BlastServer, before the installed modules are shutdown.")
public class ServerClosingEvent extends ServerEvent {

    public ServerClosingEvent(BlastServer server) {
        super(server);
    }

}
