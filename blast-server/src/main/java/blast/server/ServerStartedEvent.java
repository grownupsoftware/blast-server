/*
 * Grownup Software Limited.
 */
package blast.server;

import blast.doc.BlastEvent;
import static blast.doc.BlastEvent.EventSource.SERVER;

/**
 * Server Started Event.
 *
 * @author dhudson - Mar 25, 2017 - 7:59:31 AM
 */
@BlastEvent(source = SERVER, description = "This event is produced by the BlastServer, after the installed modules have been started.")
public class ServerStartedEvent extends ServerEvent {

    public ServerStartedEvent(BlastServer server) {
        super(server);
    }

}
