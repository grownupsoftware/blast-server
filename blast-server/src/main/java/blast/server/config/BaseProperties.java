/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.exception.BlastException;

/**
 *
 * @author dhudson - Mar 16, 2017 - 1:05:17 PM
 */
public interface BaseProperties {

    public long getAsMillis(String time) throws BlastException;

    public long getAsBytes(String bytes) throws BlastException;

    public int getAsThreads(String threads) throws BlastException;
}
