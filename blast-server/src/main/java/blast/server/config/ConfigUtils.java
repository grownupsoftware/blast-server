/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.exception.BlastException;
import blast.utils.Platform;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author dhudson - Apr 19, 2017 - 8:15:15 AM
 */
public class ConfigUtils {

    public static final int KILOBYTE = 1024;
    public static final int MEGABYTE = KILOBYTE * 1024;
    public static final int GIGABYTE = MEGABYTE * 1024;

    private ConfigUtils() {
    }

    /**
     * Converts a size into bytes.
     *
     * Append the letter k or K to indicate kilobytes, or m or M to indicate
     * megabytes, g or G to represent Gigabytes.
     *
     * Fractions are also allowed, for example 8.2M
     *
     * @param number
     * @return the number representation in bytes
     * @throws BlastException
     */
    public static int byteNumberResolver(String number) throws BlastException {
        int result = 0;

        char lastChar = number.charAt(number.length() - 1);
        if (Character.isLetter(lastChar)) {
            double num;
            try {
                num = Double.parseDouble(number.substring(0, number.length() - 1));
            } catch (NumberFormatException ex) {
                throw new BlastException("Invalid number " + number);
            }
            switch (lastChar) {
                case 'M':
                case 'm':
                    result = (int) (num * MEGABYTE);
                    break;
                case 'K':
                case 'k':
                    result = (int) (num * KILOBYTE);
                    break;
                case 'G':
                case 'g':
                    result = (int) (num * GIGABYTE);
                    if (result <= 0) {
                        throw new BlastException("Invalid number too large " + number);
                    }
                    break;
                default:
                    throw new BlastException("Invalid number " + number);
            }
        } else {
            // Its bytes
            try {
                result = Integer.parseInt(number);
            } catch (NumberFormatException ex) {
                throw new BlastException("Invalid number " + number);
            }
        }
        return result;
    }

    /**
     * Checks the last character of the string to denote the unit of measure.
     *
     * D or d for days, h or H to indicate hours, or m or M to indicate minutes,
     * s or S to indicate seconds
     *
     * @param time
     * @return the number of milliseconds
     * @throws BlastException
     */
    public static long millisNumberResolver(String time) throws BlastException {
        long result = 0;

        char lastChar = time.trim().charAt(time.length() - 1);
        if (Character.isLetter(lastChar)) {
            long num;
            try {
                num = Long.parseLong(time.substring(0, time.length() - 1));
            } catch (NumberFormatException ex) {
                throw new BlastException("Invalid time " + time);
            }

            switch (lastChar) {
                case 'd':
                case 'D':
                    result = TimeUnit.DAYS.convert(num, TimeUnit.MILLISECONDS);
                    break;
                case 'h':
                case 'H':
                    result = TimeUnit.HOURS.convert(num, TimeUnit.MILLISECONDS);
                    break;
                case 'm':
                case 'M':
                    result = TimeUnit.MINUTES.convert(num, TimeUnit.MILLISECONDS);
                    break;
                case 's':
                case 'S':
                    result = TimeUnit.SECONDS.convert(num, TimeUnit.MILLISECONDS);
                    break;
                default:
                    throw new BlastException("Invalid time " + time);
            }
        } else {
            // Its millis
            try {
                result = Long.parseLong(time);
            } catch (NumberFormatException ex) {
                throw new BlastException("Invalid time " + time);
            }
        }
        return result;
    }

    /**
     * Return the number of threads represented by this String.
     *
     * Simple Arithmetic expression to determine the number of threads to use,
     * based of the number of available cores on the deployed machine.
     *
     * The number can be absolute I.E. =5, or can be arithmetic, I.E. =/2, which
     * mean half the available cores. Remember, with threads, often less means
     * more.
     *
     * With that given, =/1, =+0, =-0, =*1 all equate to the number of cores.
     *
     * @param threads
     * @return the result of the expression
     * @throws blast.exception.BlastException
     */
    public static int threadNumberResolver(String threads) throws BlastException {

        if (Character.isDigit(threads.charAt(0))) {
            // Its absolute
            try {
                return Integer.parseInt(threads);
            } catch (NumberFormatException ex) {
                throw new BlastException("Unable to resolve thread number " + threads, ex);
            }
        }

        int num;
        String value = threads.substring(1);

        try {
            num = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            throw new BlastException("Can't parse as thread count " + threads);
        }

        int result = 0;
        switch (threads.charAt(0)) {
            case '+':
                result = Platform.getNumberOfCores() + num;
                break;
            case '-':
                result = Platform.getNumberOfCores() - num;
                break;
            case '/':
                if (num == 0) {
                    throw new BlastException("Can't divid by zero, thread count");
                }
                result = Platform.getNumberOfCores() / num;
                break;
            case '*':
                result = Platform.getNumberOfCores() * num;
                break;
            default:
                throw new BlastException("Can't parse thread count " + threads);
        }

        if (result < 0) {
            throw new BlastException("Thread expression < 0 " + threads);
        }

        return result;
    }

}
