/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.json.ObjectMapperFactory;
import blast.log.BlastLogManager;
import blast.log.BlastLogger;
import blast.utils.IOUtils;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;

/**
 * Base Config Loader.
 *
 * @author dhudson - Mar 15, 2017 - 3:55:49 PM
 * @param <E> Config POJO
 */
public abstract class ConfigLoader<E extends Object> {

    private static final BlastLogger logger = BlastLogManager.getLogger(ConfigLoader.class);
    // Need a new one as we are going to intall the lower name name staregy
    private final ObjectMapper mapper;

    public ConfigLoader() {
        mapper = ObjectMapperFactory.createObjectMapper();
        mapper.setPropertyNamingStrategy(new PropertyNamingStrategy.KebabCaseStrategy());
    }

    /**
     * load a Node config file - file must be on class path
     *
     * @param fileName the name of the file
     * @return the populated property POJO or null
     */
    public E loadConfig(String fileName) {

        Class clazz = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        JavaType type = mapper.getTypeFactory().constructSimpleType(clazz, null);
        E properties = null;

        try (InputStream is = IOUtils.getResourceAsStream(fileName)) {
            properties = mapper.readValue(is, type);
        } catch (IOException ex) {
            logger.error("Failed to read config file: {}", fileName, ex);
        }

        return properties;
    }

}
