/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.exception.BlastException;
import blast.utils.StringUtils;

/**
 * Base class for JSON property loading.
 *
 * @author dhudson - Mar 16, 2017 - 7:54:23 AM
 * @param <E> Config POJO
 */
public abstract class AbstractProperties<E> extends ConfigLoader<E> {

    /**
     * System Property Key for the property name.
     *
     * This allows for override of the default name and loading of different
     * property files.
     *
     * @return the file name, or null if not implemented
     */
    protected String getPropertySystemKey() {
        return null;
    }

    /**
     * Default property name.
     *
     * @return default property name.
     */
    protected abstract String getDefaultPropertyName();

    /**
     * Return the property name.
     *
     * If the System property is populated, then that will be used, otherwise
     * the default name will be used.
     *
     * @return property name
     */
    protected String getPropertyName() {
        if (!StringUtils.nullOrEmpty(getPropertySystemKey())) {
            return System.getProperty(getPropertySystemKey(), getDefaultPropertyName());
        }
        return getDefaultPropertyName();
    }

    public long getAsMillis(String time) throws BlastException {
        return ConfigUtils.millisNumberResolver(time);
    }

    public long getAsBytes(String bytes) throws BlastException {
        return ConfigUtils.byteNumberResolver(bytes);
    }

    public int getAsThreads(String threads) throws BlastException {
        return ConfigUtils.threadNumberResolver(threads);
    }

}
