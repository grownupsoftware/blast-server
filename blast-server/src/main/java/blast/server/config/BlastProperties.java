/*
 * Grownup Software Limited.
 */
package blast.server.config;

/**
 * Loaded Config Interface.
 *
 * @author dhudson - Mar 15, 2017 - 4:56:39 PM
 */
public interface BlastProperties {

    public int getEventBusThreadSize();

    public int getBlastersThreadSize();

    public Endpoint getEndpoint();

    public int getInputBufferSize();

    public int getOutputBufferSize();

    public boolean isKeepAlive();

    public boolean isReuseAddress();

    public boolean isNoDelay();
    
    public int getSoLinger();

}
