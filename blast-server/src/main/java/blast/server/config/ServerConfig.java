/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.doc.BlastProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * POJO of the JSON config.
 *
 * @author dhudson - Mar 15, 2017 - 3:52:16 PM
 */
@JsonInclude(Include.NON_DEFAULT)
public class ServerConfig {

    public static final String DEFAULT_CONFIG_NAME = "ServerConfig.json";

    // Create new ones of these as they contain sensible defaults
    @BlastProperty(description = "Blast Endpoint Pojo")
    public Endpoint endpoint = new Endpoint();

    @BlastProperty(description = "Socket Conditioning Pojo")
    public SocketConditioning socketConditioning = new SocketConditioning();

    // Number of cores by default
    @BlastProperty(description = "Number of threads for the event bus", defaultValue = "2")
    public String eventBusThreadPoolSize = "2";

    @BlastProperty(description = "Number of threads for the blasters", defaultValue = "Number of available cores")
    public String blastersThreadPoolSize = "+0";

    public ServerConfig() {
    }

    public String getEventBusThreadPoolSize() {
        return eventBusThreadPoolSize;
    }

    public void setEventBusThreadPoolSize(String size) {
        eventBusThreadPoolSize = size;
    }

    public String getBlastersThreadPoolSize() {
        return blastersThreadPoolSize;
    }

    public void setBlastersThreadPoolSize(String size) {
        this.blastersThreadPoolSize = size;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    public SocketConditioning getSocketConditioning() {
        return socketConditioning;
    }

    public void setSocketConditioning(SocketConditioning socketConditioning) {
        this.socketConditioning = socketConditioning;
    }

    @Override
    public String toString() {
        return "ServerConfig{" + "endpoint=" + endpoint + ", socketConditioning=" + socketConditioning + ", eventBusThreadPoolSize=" + eventBusThreadPoolSize + ", blastersThreadPoolSize=" + blastersThreadPoolSize + '}';
    }

}
