/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.doc.BlastProperty;

/**
 * Socket Conditioning POJO.
 *
 * @author dhudson - Mar 15, 2017 - 4:33:23 PM
 */
public class SocketConditioning {

    @BlastProperty(description = "Socket input buffer size", defaultValue = "8k")
    public String inputBufferSize = "8k";

    @BlastProperty(description = "Socket output buffer size", defaultValue = "8k")
    public String outputBufferSize = "8k";

    @BlastProperty(description = "Sets SO_KEEPALIVE", defaultValue = "true")
    public boolean keepAlive = true;

    @BlastProperty(description = "Sets SO_REUSEADDR", defaultValue = "true")
    public boolean reuseAddress = true;

    @BlastProperty(description = "Sets TCP_NO_DELAY", defaultValue = "true")
    public boolean noDelay = true;

    @BlastProperty(description = "Socket Linger Option, 0 to turn off", defaultValue = "0")
    public int soLinger = 0;

    public SocketConditioning() {
    }

    public String getInputBufferSize() {
        return inputBufferSize;
    }

    public void setInputBufferSize(String inputBufferSize) {
        this.inputBufferSize = inputBufferSize;
    }

    public String getOutputBufferSize() {
        return outputBufferSize;
    }

    public void setOutputBufferSize(String outputBufferSize) {
        this.outputBufferSize = outputBufferSize;
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public boolean isReuseAddress() {
        return reuseAddress;
    }

    public void setReuseAddress(boolean reuseAddress) {
        this.reuseAddress = reuseAddress;
    }

    public boolean isNoDelay() {
        return noDelay;
    }

    public void setNoDelay(boolean noDelay) {
        this.noDelay = noDelay;
    }

    public int getSoLinger() {
        return soLinger;
    }

    public void setSoLinger(int soLinger) {
        this.soLinger = soLinger;
    }

    @Override
    public String toString() {
        return "SocketConditioning{" + "inputBufferSize=" + inputBufferSize + ", outputBufferSize=" + outputBufferSize + ", keepAlive=" + keepAlive + ", reuseAddress=" + reuseAddress + ", noDelay=" + noDelay + ", soLinger=" + soLinger + '}';
    }

}
