/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.doc.BlastProperty;
import blast.utils.Platform;

/**
 * Server Properties.
 *
 * Hangs off the server.
 *
 * @author dhudson - Mar 15, 2017 - 4:56:52 PM
 */
public class BlastPropertiesImpl extends AbstractProperties<ServerConfig> implements BlastProperties, BaseProperties {

    private static final int DEFAULT_BUF_SIZE = 8 * ConfigUtils.KILOBYTE;
    private static final int DEFAULT_THREAD_POOL_SIZE = Platform.getNumberOfCores();

    BlastLogger logger = BlastLogger.createLogger();

    @BlastProperty(description = "The System Property Key to look for a config file", defaultValue = "blast-server.config")
    private static final String PROPERTY_KEY = "blast-server.config";

    @BlastProperty(description = "The default file name for the blast config", defaultValue = "blast-server.json")
    private static final String DEFAULT_NAME = "blast-server.json";

    private ServerConfig serverConfig;

    public BlastPropertiesImpl() {
        // Sensible defaults
        serverConfig = new ServerConfig();
    }

    public void load() {
        serverConfig = loadConfig(getPropertyName());
    }

    @Override
    protected String getDefaultPropertyName() {
        return DEFAULT_NAME;
    }

    @Override
    protected String getPropertySystemKey() {
        return PROPERTY_KEY;
    }

    @Override
    public int getBlastersThreadSize() {
        try {
            return getAsThreads(serverConfig.getBlastersThreadPoolSize());
        } catch (BlastException ex) {
            logger.warn("Unable to use {} for Blasters Thread Size, using {} instead.", serverConfig.getBlastersThreadPoolSize(), DEFAULT_THREAD_POOL_SIZE);
            return DEFAULT_THREAD_POOL_SIZE;
        }
    }

    @Override
    public Endpoint getEndpoint() {
        return serverConfig.getEndpoint();
    }

    @Override
    public int getInputBufferSize() {
        String bytes = serverConfig.getSocketConditioning().getInputBufferSize();
        try {
            return (int) getAsBytes(bytes);
        } catch (BlastException ex) {
            logger.warn("Unable to use {} for Input Buffer Size, using {} instead", bytes, DEFAULT_BUF_SIZE);
        }
        return DEFAULT_BUF_SIZE;
    }

    @Override
    public int getOutputBufferSize() {
        String bytes = serverConfig.getSocketConditioning().getOutputBufferSize();
        try {
            return (int) getAsBytes(bytes);
        } catch (BlastException ex) {
            logger.warn("Unable to use {}  for Output Buffer Size, using {} instead", bytes, DEFAULT_BUF_SIZE);
        }
        return DEFAULT_BUF_SIZE;
    }

    @Override
    public boolean isKeepAlive() {
        return serverConfig.getSocketConditioning().isKeepAlive();
    }

    @Override
    public boolean isReuseAddress() {
        return serverConfig.getSocketConditioning().isReuseAddress();
    }

    @Override
    public boolean isNoDelay() {
        return serverConfig.getSocketConditioning().isNoDelay();
    }

    @Override
    public int getSoLinger() {
        return serverConfig.getSocketConditioning().getSoLinger();
    }

    @Override
    public String toString() {
        return "BlastPropertiesImpl{" + serverConfig + '}';
    }

    @Override
    public int getEventBusThreadSize() {
        try {
            return getAsThreads(serverConfig.getEventBusThreadPoolSize());
        } catch (BlastException ex) {
            logger.warn("Unable to use {} for Event Bus Thread Size, using {} instead.", serverConfig.getEventBusThreadPoolSize(), DEFAULT_THREAD_POOL_SIZE);
            return DEFAULT_THREAD_POOL_SIZE;
        }
    }

}
