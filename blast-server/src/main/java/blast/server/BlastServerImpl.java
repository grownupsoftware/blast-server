/*
 * Grownup Software Limited.
 */
package blast.server;

import blast.blaster.Blaster;
import blast.blaster.BlasterImpl;
import blast.client.BlastServerClient;
import blast.client.ClientManager;
import blast.client.ClientManagerModule;
import blast.Controllable;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.eventbus.BlastEventBus;
import blast.eventbus.BlastEventBusImpl;
import blast.message.BroadcastEvent;
import blast.module.BlastModule;
import blast.server.config.BlastProperties;
import blast.server.config.BlastPropertiesImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Blast Server.
 *
 * A blast server will have an event bus, blasters and client manager.
 *
 * @author dhudson - Mar 16, 2017 - 1:32:09 PM
 */
public class BlastServerImpl implements BlastServer {

    private final BlastLogger logger = BlastLogger.createLogger();

    private BlastProperties properties;
    private final ClientManager clientManager;
    private boolean isStarted = false;
    private final BlastEventBus eventBus;

    private final Blaster blaster;
    private final HashMap<String, BlastModule> modules;
    private ObjectMapper objectMapper;

    public BlastServerImpl() {
        modules = new LinkedHashMap<>();

        // I think that we will always need these, but we could move this to the Builder
        eventBus = new BlastEventBusImpl();
        installModule(eventBus);

        clientManager = new ClientManagerModule();
        installModule(clientManager);

        blaster = new BlasterImpl();
        installModule(blaster);
    }

    @Override
    public void startup() throws BlastException {
        if (!isStarted) {
            logger.info("Starting Blast Server ...");

            // If this has come from the builder, this will not happen.
            if (properties == null) {
                logger.info("Properties not set, using defaults");
                // Using default properties
                properties = new BlastPropertiesImpl();
            }

            // Modules can depend on modules, so lets get the current list
            List<BlastModule> currentModules = new ArrayList<>(modules.values());
            // Call config on the modules
            for (BlastModule module : currentModules) {
                registerModule(module);
            }

            // Need to be after the configure, as this is where events gets registered.
            eventBus.post(new ServerStartingEvent(this));

            currentModules = new ArrayList<>(modules.values());
            for (BlastModule module : currentModules) {
                if (module instanceof Controllable) {
                    ((Controllable) module).startup();
                }
            }

            isStarted = true;
            eventBus.post(new ServerStartedEvent(this));
        }
    }

    @Override
    public void shutdown() {
        if (isStarted) {
            logger.info("Stopping Blast Server ...");

            eventBus.post(new ServerClosingEvent(this));

            for (BlastModule module : modules.values()) {
                if (module instanceof Controllable) {
                    ((Controllable) module).shutdown();
                }
            }

            isStarted = false;
        }
    }

    @Override
    public void setProperties(BlastProperties properties) {
        this.properties = properties;
    }

    @Override
    public BlastProperties getProperties() {
        return properties;
    }

    @Override
    public BlastEventBus getEventBus() {
        return eventBus;
    }

    @Override
    public BlastModule requireModule(String ID, Class<? extends BlastModule> module) throws BlastException {
        BlastModule existing = modules.get(ID);
        if (existing != null) {
            return existing;
        }

        // Lets create a new one
        try {
            existing = module.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new BlastException("Unable to create new instance of Module " + ID);
        }

        registerModule(existing);
        return existing;
    }

    @Override
    public BlastServer registerEventListener(Object eventListener) {
        eventBus.register(eventListener);
        return this;
    }

    @Override
    public final BlastServer installModule(BlastModule module) {
        modules.put(module.getModuleID(), module);
        return this;
    }

    @Override
    public final BlastServer registerModule(BlastModule module) throws BlastException {
        module.configure(this);
        installModule(module);
        return this;
    }

    @Override
    public void blast(String message) {
        eventBus.post(new BroadcastEvent(message));
    }

    @Override
    public List<BlastServerClient> getClients() {
        return clientManager.getClients();
    }

    @Override
    public BlastServerClient getClientByID(String id) {
        return clientManager.getClientByID(id);
    }

    @Override
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void setObjectMapper(ObjectMapper mapper) {
        objectMapper = mapper;
    }

    @Override
    public BlastModule getModule(String moduleID) {
        return modules.get(moduleID);
    }

    @Override
    public void postEvent(Object event) {
        eventBus.post(event);
    }

    @Override
    public boolean hasModule(String ID) {
        return modules.containsKey(ID);
    }
}
