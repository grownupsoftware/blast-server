/*
 * Grownup Software Limited.
 */
package blast.blaster;

import blast.Controllable;
import blast.exception.BlastException;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Monitors the Blast Workers.
 *
 * @author dhudson - Mar 27, 2017 - 8:50:29 AM
 */
public class BlasterMonitor extends Thread implements Controllable {

    public boolean isRunning = false;

    private final Set<BlasterWorker> workers = new CopyOnWriteArraySet<>();

    public BlasterMonitor(Collection<BlasterWorker> workers) {
        setName("Blaster Monitor");
        this.workers.addAll(workers);
    }

    @Override
    public void run() {
        isRunning = true;
        while (isRunning) {
            try {
                synchronized (this) {
                    wait(100);
                }
                for (BlasterWorker worker : workers) {
                    if (worker.isTakingTooLong()) {
                        worker.getServiceThread().interrupt();
                    }
                }
            } catch (InterruptedException ex) {
                isRunning = false;
            }
        }
    }

    @Override
    public void startup() throws BlastException {
        start();
    }

    @Override
    public void shutdown() {
        isRunning = false;
        synchronized (this) {
            interrupt();
        }
    }

}
