/*
 * Grownup Software Limited.
 */
package blast.blaster;

import blast.client.BlastServerClient;
import blast.doc.BlastEvent;
import static blast.doc.BlastEvent.EventSource.SERVER;
import java.util.List;

/**
 *
 * @author dhudson - Apr 27, 2017 - 11:05:39 AM
 */
@BlastEvent(source = SERVER, description = "This event contains what was on the blaster queue, when the client closed.")
public class RemovedBlasterEntriesEvent {

    private final BlastServerClient serverClient;
    private final List<BlasterQueueEvent> removedEvents;

    public RemovedBlasterEntriesEvent(BlastServerClient client, List<BlasterQueueEvent> events) {
        serverClient = client;
        removedEvents = events;
    }

    public BlastServerClient getClient() {
        return serverClient;
    }

    public List<BlasterQueueEvent> getRemovedEvents() {
        return removedEvents;
    }
}
