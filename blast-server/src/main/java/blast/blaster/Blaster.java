/*
 * Grownup Software Limited.
 */
package blast.blaster;

import blast.Controllable;
import blast.module.BlastModule;
import java.util.Set;
import java.util.concurrent.ExecutorService;

/**
 *
 * @author dhudson - Mar 24, 2017 - 10:18:27 AM
 */
public interface Blaster extends Controllable, BlastModule {

    public BlasterQueue getBlasterQueue();

    public ExecutorService getBlasterServer();

    public Set<BlasterWorker> getBlasters();

}
