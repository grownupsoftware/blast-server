/*
 * Grownup Software Limited.
 */
package blast.server.config;

import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.utils.Platform;
import org.junit.Test;

/**
 *
 * @author dhudson - Mar 16, 2017 - 8:05:19 AM
 */
public class ServerPropertyTester {

    private static final BlastLogger logger = BlastLogger.createLogger();

    @Test
    /**
     * Test loading general blast-server.json.
     */
    public void loadPropertyTest() throws BlastException {

        BlastPropertiesImpl properties = new BlastPropertiesImpl();
        properties.load();

        assert (properties.getBlastersThreadSize() == 5);
        assert (properties.getEventBusThreadSize() == 2);
        assert (properties.getEndpoint().getName().equals("Test"));
    }

    @Test
    public void asBytesTest() throws BlastException {
        assert (ConfigUtils.byteNumberResolver("12") == 12);
        assert (ConfigUtils.byteNumberResolver("1m") == ConfigUtils.MEGABYTE);
        assert (ConfigUtils.byteNumberResolver("8k") == (ConfigUtils.KILOBYTE * 8));
        assert (ConfigUtils.byteNumberResolver("1.5K") == (int) (ConfigUtils.KILOBYTE * 1.5));
    }

    @Test
    public void asThreadsTest() {

        int cores = Platform.getNumberOfCores();

        try {
            assert (2 == ConfigUtils.threadNumberResolver("2"));
            assert ((cores + 1) == ConfigUtils.threadNumberResolver("+1"));
            assert ((cores * 2) == ConfigUtils.threadNumberResolver("*2"));
            assert ((cores / 2) == ConfigUtils.threadNumberResolver("/2"));
            if (cores > 1) {
                // Will not pass on a single core machine, as the result is less than 1
                assert ((cores - 1) == ConfigUtils.threadNumberResolver("-1"));
            }
        } catch (BlastException ex) {
            assert (false);
        }
    }

}
