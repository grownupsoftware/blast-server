/*
 * Grownup Software Limited.
 */
package blast.client;

import blast.log.BlastLogger;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;

/**
 *
 * @author dhudson - Apr 4, 2017 - 3:28:24 PM
 */
public class ShortIDTester {

    private static final BlastLogger logger = BlastLogger.createLogger();

    @Test
    public void shortIDTest() {

        Set<String> ids = new HashSet<>(1000);

        for (int i = 0; i < 1000; i++) {
            assert (ids.add(BlastServerClientUtils.getShortClientID()));
        }
    }
}
