/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import blast.log.BlastLogger;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author dhudson - Apr 10, 2017 - 11:10:22 AM
 */
public class SortedListTester {

    private static final BlastLogger logger = BlastLogger.createLogger();

    @Test
    public void sortedMethodTest() {
        ListenerRegistra registra = new ListenerRegistra();
        FooListener fooListener1 = new FooListener();
        registra.register(fooListener1);

        List<ListenerMethod> methods = registra.getListenersFor(Foo.class);

        assert (methods.size() == 5);

        for (ListenerMethod method : methods) {
            logger.info(" Method Name {} ", method.getMethod().getName());
        }

        assert (methods.get(0).getMethod().getName().equals("method1"));
        assert (methods.get(1).getMethod().getName().equals("method2"));
        assert (methods.get(2).getMethod().getName().equals("method3"));
        assert (methods.get(3).getMethod().getName().equals("method4"));
        assert (methods.get(4).getMethod().getName().equals("method99"));

        // Register the same thing again
        registra.register(fooListener1);

        methods = registra.getListenersFor(Foo.class);
        assert (methods.size() == 5);

        registra.register(new FooListener());
        methods = registra.getListenersFor(Foo.class);

        assert (methods.size() == 10);

        registra.unregister(fooListener1);
        methods = registra.getListenersFor(Foo.class);

        assert (methods.size() == 5);
    }
}
