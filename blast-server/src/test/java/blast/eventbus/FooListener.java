/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import blast.log.BlastLogger;
import blast.server.BlastServer;
import java.util.ArrayList;

/**
 *
 * @author dhudson - Apr 10, 2017 - 11:24:40 AM
 */
public class FooListener {

    private static final BlastLogger logger = BlastLogger.createLogger();

    @OnEvent(order = 10)
    public void method1(Foo foo) {
        logger.info("Execute method1");
    }

    @OnEvent(order = 30)
    public void method3(Foo foo) {
        logger.info("Execute method3");
    }

    @OnEvent
    public void method4(Foo foo) {
        logger.info("Execute method4");
    }

    @OnEvent(order = 20)
    public void method2(Foo foo) {
        logger.info("Execute method2");
    }

    @OnEvent(order = 99)
    public void method99(Foo foo, BlastServer blastServer) {
        logger.info("Execute method99");
    }

    @OnEvent
    public void invalid1() {
    }

    @OnEvent
    public void invalid2(Foo foo, ArrayList<String> list) {
    }
}
