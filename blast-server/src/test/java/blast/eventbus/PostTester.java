/*
 * Grownup Software Limited.
 */
package blast.eventbus;

import blast.Blast;
import blast.exception.BlastException;
import blast.server.BlastServer;
import org.junit.Test;

/**
 *
 * @author dhudson - Apr 10, 2017 - 3:41:18 PM
 */
public class PostTester {

    @Test
    public void postTest() throws BlastException {

        BlastServer server = Blast.blast();
        server.startup();

        FooListener listener = new FooListener();

        BlastEventBus eventBus = server.getEventBus();

        eventBus.register(listener);

        eventBus.post(new Foo());
    }
}
