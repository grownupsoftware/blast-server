/*
 * Grownup Software Limited.
 */
package blast.jetty.engine;

import blast.client.WebSocketRequestDetailsImpl;
import blast.jetty.client.JettyServerClient;
import blast.server.BlastServer;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;

/**
 *
 * @author dhudson - Mar 28, 2017 - 2:56:41 PM
 */
public class BlastWebSocketCreator implements WebSocketCreator {

    private final BlastServer server;

    public BlastWebSocketCreator(BlastServer server) {
        this.server = server;
    }

    @Override
    public Object createWebSocket(ServletUpgradeRequest request, ServletUpgradeResponse response) {
        WebSocketRequestDetailsImpl details = new WebSocketRequestDetailsImpl();
        details.setHeaders(request.getHeaders());
        details.setParams(request.getParameterMap());
        details.setQueryString(request.getQueryString());
        details.setRequestPath(request.getRequestPath());
        details.setOrigin(request.getOrigin());
        details.setRequestURI(request.getRequestURI());
        details.setRemoteAddress(request.getRemoteAddress());
        details.resolveRemoteAddress();
        
        return new JettyServerClient(server, details);
    }

}
