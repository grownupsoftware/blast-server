/*
 * Grownup Software Limited.
 */
package blast.jetty.engine;

import blast.BlastConstants;
import blast.Controllable;
import blast.exception.BlastException;
import blast.module.BlastModule;
import blast.server.BlastServer;
import blast.server.config.BlastProperties;
import blast.server.config.Endpoint;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.Server;

/**
 *
 * @author dhudson - Mar 28, 2017 - 11:50:25 AM
 */
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class JettyEngine implements BlastModule, Controllable {

    public static final String JETTY_ENGINE_MODULE_ID = "co.gusl.jetty.engine";

    private Server jettyServer;

    @Override
    public void configure(BlastServer server) throws BlastException {
        BlastProperties properties = server.getProperties();
        jettyServer = new Server();

        HttpConfiguration config = new HttpConfiguration();
        config.setOutputBufferSize(properties.getOutputBufferSize());

        ServerConnector connector = new ServerConnector(jettyServer);
        Endpoint endpoint = properties.getEndpoint();

        connector.setAcceptQueueSize(endpoint.getBacklog());
        connector.setPort(endpoint.getPort());
        connector.setReuseAddress(properties.isReuseAddress());
        connector.setSoLingerTime(properties.getSoLinger());

        jettyServer.addConnector(connector);

        ServletHolder holder = new ServletHolder();
        holder.setServlet(new JettyBlastServlet(server));

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(holder, BlastConstants.BLAST_ROUTE_PATH);

        jettyServer.setHandler(context);

    }

    @Override
    public void startup() throws BlastException {
        try {
            jettyServer.start();
        } catch (Exception ex) {
            throw new BlastException("Unable to start Jetty", ex);
        }
    }

    @Override
    public void shutdown() {
        if (jettyServer != null) {
            try {
                jettyServer.stop();
            } catch (Exception ignore) {
            }
        }
    }

    @Override
    public String getModuleID() {
        return JETTY_ENGINE_MODULE_ID;
    }

}
