![Alt text](./resources/images/full-splash-trans-500.png)
# Blast

Blast is a lightweight edge server that sends and receives messages via WebSockets.
Blast is protocol less, so anything that can open a WebSocket is a client.
Blast can be stand alone, or embedded into your application.
Many Blast servers can be instantiated in one JVM, with a share nothing principle. (No Singletons were harmed during the making of Blast).
Each blast server has its own event bus, events can be published, and subscriptions can be ordered and chained. 

A wise man once said, 
>If you can't understand the code, then the pattern is wrong.

and we believe that to be the same with software.
Blast is all about keeping things simple, and allowing for modules to give ever increasing complexity.


Implementations, include [Jetty](./jetty/README.md), [Vert.x](./vertx/README.md), [Tomcat](./tomcat/README.md), 
[Undertow](./undertow/README.md), [Ninja](./ninja/README.md) and Akka.

# Getting Started.

```
    git clone
    cd Blast
    gradle wrapper
    ./gradlew compileJava
```

## 10 Second Overview.

Broadcast a message to all connected clients.

### Server Side.

```java
    BlastServer blast = Blast.blast();
    ...
    blast.blast("Hello from Blast!");
```

### Client Side.

```javascript
    ws = new WebSocket("ws://host:port/blast");
    ws.onmessage = function (evt) {
        document.getElementById('MessageBox').value = evt.data;
    };
```

## 20 Second Overview.

Blast uses a simple RPC approach for client server communications.  In this example the client sends a RPC command (`foo`) with params, 
but a server can also send a RPC to the client.  As you can see the payload of this message is JSON, but interactions are with POJO's. 

### Server Side.

```java
    BlastServer blast = Blast.blast(new JettyEngine());
    blast.registerCommand("foo", FooEvent.class, FooMessage.class, new FooEventHandler());
```
And the handler.

```java
public class FooEventHandler {

    @OnEvent
    public void handleFooCommand(FooEvent ev) {
        FooMessage message = ev.getCommandData();
        String params = message.getParams();
        BlastServerClient client = ev.getClient();
    }
}   

```

As this event is now published on the internal event bus, you can have many independent listeners for the FooEvent.

### Client Side.

```javascript
    ws = new WebSocket("ws://host:port/blast");
    ws.send(" { 'cmd': 'foo', 'params': 'bar...' } ");
```

## 30 Second Overview.

Now that we have the concept of RPC's, we can .....

### Server Side.

```java
    BlastServer blast = Blast.blast(new UndertowEngine(), ... );
```

# Documentation.

Documentation can be found [here](http://www.gusl.co/blast)

# Contact
Any issues, please report to blast@gusl.co

