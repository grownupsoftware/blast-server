
![Alt text](../resources/images/b-circle-trans-100.png) *on* ![Alt test](http://tomcat.apache.org/images/tomcat.png)

Built using Tomcat version 8.5.12


# Embedded

## JSR 356

As Tomcat likes to use the JSR 356, then just include `BlastServerEndpoint` in your war.

If you require the Remote IP address of the WebSocket, then the Blast Filter will also need to be
added. See the BlastIPFilter.

# Stand alone

To run the Standalone version of blast use ..

```java
     BlastServer blast = Blast.blast(new TomcatEngine())
```
This will create a Tomcat instance and configure using the properties from Blast Properties.
As the `TomcatEngine` is a `Controllable` Blast will start and stop the engine.

## Gradle Runner
`./gradlew tomcat:tomcat-engine:run`
