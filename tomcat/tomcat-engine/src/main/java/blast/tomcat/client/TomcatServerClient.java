/*
 * Grownup Software Limited.
 */
package blast.tomcat.client;

import blast.client.AbstractBlastServerClient;
import blast.client.ClosingReason;
import blast.client.WebSocketRequestDetails;
import blast.server.BlastServer;
import java.io.IOException;
import javax.websocket.Session;
import org.apache.tomcat.util.http.fileupload.IOUtils;

/**
 *
 * @author dhudson - Apr 3, 2017 - 10:50:18 AM
 */
public class TomcatServerClient extends AbstractBlastServerClient {

    private final Session session;

    public TomcatServerClient(Session session, BlastServer server, WebSocketRequestDetails details) {
        super(server, details);
        this.session = session;
    }

    @Override
    public void close(ClosingReason reason) {
        if (session.isOpen()) {
            IOUtils.closeQuietly(session);
        }
        postClientClosed(reason);
    }

    @Override
    public String getClientID() {
        return session.getId();
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        if (session.isOpen()) {
            session.getBasicRemote().sendText(new String(bytes));
        }
    }

    @Override
    public void queueMessage(byte[] bytes) {
        postQueuedMessageEvent(bytes);
    }

}
