/*
 * Grownup Software Limited.
 */
package blast.tomcat.engine;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Wrapper so that we can add an headers.
 *
 * @author dhudson - Apr 4, 2017 - 11:57:54 AM
 */
public class BlastRequestWrapper extends HttpServletRequestWrapper {

    // Make it two for now, but there might be more headers.
    private final HashMap<String, String> headers = new HashMap<>(2);

    public BlastRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    public void addHeader(String name, String value) {
        headers.put(name, value);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        List<String> names = Collections.list(super.getHeaderNames());
        headers.keySet().forEach((name) -> {
            names.add(name);
        });
        return Collections.enumeration(names);
    }

    @Override
    public String getHeader(String name) {
        if (headers.containsKey(name)) {
            return headers.get(name);
        }
        return super.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        List<String> values = Collections.list(super.getHeaders(name));
        if (headers.containsKey(name)) {
            values.add(headers.get(name));
        }
        return Collections.enumeration(values);
    }

}
