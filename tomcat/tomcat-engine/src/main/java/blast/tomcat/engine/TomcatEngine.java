/*
 * Grownup Software Limited.
 */
package blast.tomcat.engine;

import blast.Controllable;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.module.BlastModule;
import blast.server.BlastServer;
import blast.server.config.BlastProperties;
import blast.server.config.Endpoint;
import blast.tomcat.server.BlastTomcatServletConfig;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.FilterDef;
import org.apache.tomcat.util.descriptor.web.FilterMap;

/**
 *
 * @author dhudson - Mar 31, 2017 - 10:41:11 AM
 */
public class TomcatEngine implements BlastModule, Controllable {

    public static final String TOMCAT_ENGINE_MODULE_ID = "co.gusl.tomcat.engine";

    private static final BlastLogger logger = BlastLogger.createLogger();
    private static final String SERVLET_NAME = "blast";

    private Tomcat tomcat;

    @Override
    public void configure(BlastServer server) throws BlastException {
        tomcat = new Tomcat();
        Endpoint endpoint = server.getProperties().getEndpoint();
        tomcat.setPort(endpoint.getPort());
        Context context = tomcat.addContext("", null);

        FilterDef filterDef = new FilterDef();
        filterDef.setFilterClass(BlastIPFilter.class.getName());
        filterDef.setFilterName(BlastIPFilter.class.getSimpleName());
        context.addFilterDef(filterDef);

        FilterMap filterMap = new FilterMap();
        filterMap.setFilterName(BlastIPFilter.class.getSimpleName());
        filterMap.addURLPattern("/*");
        context.addFilterMap(filterMap);

        context.addApplicationListener(BlastTomcatServletConfig.class.getName());

        // Lets configure the connector
        BlastProperties properties = server.getProperties();
        Connector connector = tomcat.getConnector();
        connector.setProperty("acceptCount", Integer.toString(endpoint.getBacklog()));
        connector.setProperty("tcpNoDelay", Boolean.toString(properties.isNoDelay()));
        connector.setProperty("socketBuffer", Integer.toString(properties.getOutputBufferSize()));
        connector.setProperty("socket.rxBufSize", Integer.toString(properties.getInputBufferSize()));
        connector.setProperty("socket.txBufSize", Integer.toString(properties.getOutputBufferSize()));
        connector.setProperty("socket.soKeepAlive", Boolean.toString(properties.isKeepAlive()));
        connector.setProperty("socket.soReuseAddress", Boolean.toString(properties.isReuseAddress()));

        // Tomcat uses -1 as off
        if (properties.getSoLinger() > 0) {
            connector.setProperty("socket.soLingerOn", Boolean.toString(true));
            connector.setProperty("socket.soLingerTime", Integer.toString(properties.getSoLinger()));
        } else {
            connector.setProperty("socket.soLingerOn", Boolean.toString(false));
        }

        Tomcat.addServlet(context, SERVLET_NAME, new DefaultServlet());
        context.addServletMappingDecoded("/", SERVLET_NAME);
    }

    @Override
    public void startup() throws BlastException {
        if (tomcat != null) {
            try {
                tomcat.start();
            } catch (LifecycleException ex) {
                logger.warn("Unable to start Tomcat", ex);
                throw new BlastException("Unable to start Tomcat", ex);
            }
        }
    }

    @Override
    public void shutdown() {
        if (tomcat != null) {
            try {
                tomcat.stop();
            } catch (LifecycleException ignore) {

            }
        }
    }

    @Override
    public String getModuleID() {
        return TOMCAT_ENGINE_MODULE_ID;
    }

}
