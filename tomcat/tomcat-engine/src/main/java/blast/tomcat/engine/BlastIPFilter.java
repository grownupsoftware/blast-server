/*
 * Grownup Software Limited.
 */
package blast.tomcat.engine;

import static blast.BlastConstants.REMOTE_ADDRESS_KEY;
import static blast.BlastConstants.REMOTE_PORT_KEY;
import blast.log.BlastLogger;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * IP Filter so that we can capture the Remote IP Address of the inbound
 * request.
 *
 * @author dhudson - Apr 4, 2017 - 11:02:27 AM
 */
public class BlastIPFilter implements Filter {

    private static final BlastLogger logger = BlastLogger.createLogger();

    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fc) throws IOException, ServletException {
        // Capture the IP address.
        // This is all far too bloody hard for something simple
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            BlastRequestWrapper wrapper = new BlastRequestWrapper(httpRequest);

            // Add extra headers
            wrapper.addHeader(REMOTE_ADDRESS_KEY, httpRequest.getRemoteAddr());
            wrapper.addHeader(REMOTE_PORT_KEY, Integer.toString(httpRequest.getRemotePort()));

            fc.doFilter(wrapper, response);
        } else {
            fc.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }

}
