/*
 * Grownup Software Limited.
 */
package blast.tomcat.server;

import blast.Blast;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import blast.module.echo.EchoModule;
import blast.module.ping.PingModule;
import blast.server.BlastServer;
import blast.tomcat.engine.BlastWebSocketConfigurator;
import blast.tomcat.engine.TomcatEngine;

/**
 *
 * @author dhudson - Mar 31, 2017 - 10:40:03 AM
 */
public class BlastTomcatServer {
    
    private static final BlastLogger logger = BlastLogger.createLogger();
    
    public static void main(String[] args) {
        
        BlastServer blastServer = Blast.blast(new TomcatEngine(), new PingModule(), new EchoModule());

        // Register before starting
        Blast.storeBlastInstance(BlastWebSocketConfigurator.BLAST_INSTANCE_KEY, blastServer);
        
        try {
            blastServer.startup();
        } catch (BlastException ex) {
            logger.warn("Unable to start Blast Server", ex);
        }
        
    }
    
}
